# redux-sample

Redux calculator example

### Requirements
  - `node 5.0.0` and higher!

### Usage

##### Getting Started:

Install dependencies.
```
npm install
```

Run development server, complete with DevTools and related configuration.
```
npm start
```

```
open http://localhost:3000/
```

---
Based on [https://github.com/tsaiDavid/simple-redux-boilerplate](simple-redux-boilerplate)