import { CALCULATOR_OPERATION, CALCULATOR_OPERAND } from '../constants/ActionTypes';

export function operation(operation) {
  return {
    type: CALCULATOR_OPERATION,
    operation: operation
  };
}

export function operand() {
  return {
    type: CALCULATOR_OPERAND
  };
}

export function equals() {
  return {
    type: CALCULATOR_EQUALS
  };
}