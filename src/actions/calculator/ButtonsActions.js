import { CALCULATOR_NUMBER, CALCULATOR_CLEAR, CALCULATOR_OPERATION, CALCULATOR_EQUAL } from '../../constants/ActionTypes';

export function keyboardNumber(value) {
  return {
    type: CALCULATOR_NUMBER,
    value
  };
}

export function keyboardClear() {
  return {
    type: CALCULATOR_CLEAR
  };
}

export function keyboardEqual() {
  return {
    type: CALCULATOR_EQUAL
  };
}

export function keyboardOperation(operation) {
  return {
    type: CALCULATOR_OPERATION,
    operation
  };
}

