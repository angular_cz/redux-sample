import React from 'react';

import Display from '../containers/Display'
import Buttons from '../containers/Buttons'

export default function() {
  return <div className="calculator">
    <Display />
    <Buttons />
  </div>
}