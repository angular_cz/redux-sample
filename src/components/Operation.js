import React from 'react';

let Operation = (props) => {
  const onClick = () => {
    props.onClick(props.name)
  };

  return <button type="button"
                 className="operation"
                 name={props.name}
                 onClick={onClick}>
    {props.symbol}
  </button>
};

Operation.propTypes = {
  name: React.PropTypes.string.isRequired,
  symbol: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func.isRequired
};

export default Operation;