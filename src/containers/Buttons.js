import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from '../components/Button'
import Operation from '../components/Operation'

import * as buttonActions from '../actions/calculator/ButtonsActions'

const Buttons = (props) => {

  return <div id="buttons">
    <div>
      <Button value={1} onClick={props.onNumberClick}/>
      <Button value={4} onClick={props.onNumberClick}/>
      <Button value={7} onClick={props.onNumberClick}/>
      <Operation name="clear" symbol="C" onClick={props.onClearClick}/>
    </div>

    <div>
      <Button value={2} onClick={props.onNumberClick}/>
      <Button value={5} onClick={props.onNumberClick}/>
      <Button value={8} onClick={props.onNumberClick}/>
      <Button value={0} onClick={props.onNumberClick}/>
    </div>

    <div>
      <Button value={3} onClick={props.onNumberClick}/>
      <Button value={6} onClick={props.onNumberClick}/>
      <Button value={9} onClick={props.onNumberClick}/>
      <Operation name="equals" symbol="=" onClick={props.onEqualClick}/>
    </div>

    <div>
      <Operation name="add" symbol="+" onClick={props.onOperationClick}/>
      <Operation name="sub" symbol="-" onClick={props.onOperationClick}/>
      <Operation name="mul" symbol="x" onClick={props.onOperationClick}/>
      <Operation name="div" symbol="/" onClick={props.onOperationClick}/>
    </div>
  </div>
};

Buttons.propTypes = {
  onClearClick: React.PropTypes.func.isRequired,
  onEqualClick: React.PropTypes.func.isRequired,
  onNumberClick: React.PropTypes.func.isRequired,
  onOperationClick: React.PropTypes.func.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    onClearClick: () => { dispatch(buttonActions.keyboardClear())},
    onEqualClick: () => { dispatch(buttonActions.keyboardEqual())},
    onNumberClick: (number) => { dispatch(buttonActions.keyboardNumber(number))},
    onOperationClick: (operation) => { dispatch(buttonActions.keyboardOperation(operation)) }
  }
}

export default connect(null, mapDispatchToProps)(Buttons);