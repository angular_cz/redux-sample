import React from 'react';
import {connect} from 'react-redux';

const Display = (props) => {
  return <div className="display">
    <input type="number"
           value={props.value}
           readOnly/>
  </div>
};

Display.propTypes = {
  value: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
  ]).isRequired
};

function mapStateToProps(state) {
  return {
    value: state.calculator.displayValue
  }
}

export default connect(mapStateToProps)(Display);