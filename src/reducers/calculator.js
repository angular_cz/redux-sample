import { CALCULATOR_NUMBER, CALCULATOR_CLEAR, CALCULATOR_OPERATION, CALCULATOR_EQUAL } from '../constants/ActionTypes';

import { calculator } from '../calculations/calculator';

const initialState = {
  displayValue: 0,
  operand: null,
  result: null
};

const concatToString = (number, value) => {
  if (!number) {
    return value;
  }

  return Number(String(number).concat(value));
};

export default function counter(state = initialState, action) {
  switch (action.type) {
    case CALCULATOR_NUMBER:
      const currentNumber = concatToString(state.operand, action.value);
      return {
        ...state,
        displayValue: currentNumber,
        operand: currentNumber
      };

    case CALCULATOR_CLEAR:
      return initialState;

    case CALCULATOR_OPERATION:
      return {
        ...state,
        result: state.result || state.operand,
        operation: action.operation,
        operand: 0
      };

    case CALCULATOR_EQUAL:
      const result = calculator.calculate(state.operation, state.result, state.operand);
      return {
        ...state,
        displayValue: result,
        result
      };

    default:
      return state;
  }
}
